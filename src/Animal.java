public class Animal {

    private String name;

    private String color;

    public Animal(String name,String color){
        this.name = name;
        this.color = color;

    }

    public Animal() {

    }

    public String getAnimalName(){
        //this keyword refers to the object/instance where the constructor or setter/getter is.
        return this.name;
    }

    public void setName(String nameParams){
        this.name = nameParams;
    }
    public String getAnimalColor(){
        //this keyword refers to the object/instance where the constructor or setter/getter is.
        return this.color;
    }

    public void setColor(String colorParams){
        this.color = colorParams;
    }

    public void call(){
        System.out.println("Hi! My name is " + getAnimalName());
    }









}
